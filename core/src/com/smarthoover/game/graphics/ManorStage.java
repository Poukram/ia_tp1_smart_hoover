package com.smarthoover.game.graphics;


import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.utils.viewport.Viewport;
import com.smarthoover.game.agent.Aspirobot;
import com.smarthoover.game.agent.objects.SelfActor;
import com.smarthoover.game.environment.Manor;
import com.smarthoover.game.environment.objects.DiamondActor;
import com.smarthoover.game.environment.objects.DirtActor;
import com.smarthoover.game.utils.EnvReport;

import java.util.Observable;
import java.util.Observer;

public class ManorStage  extends Stage implements Observer {
    private int _count = 0;
    private final Group bg = new Group();
    private final Group fg = new Group();

    private void init(){
        this.addActor(bg);
        this.addActor(fg);
    }

    public ManorStage(){
        super();
        init();
    }


    public ManorStage(Viewport vp){
        super(vp);
        init();
    }

    public ManorStage(Viewport vp, Batch b){
        super(vp, b);
        init();
    }

    @Override
    public void update(Observable o, Object arg) {
        System.out.println("Received update number " + _count++ + " from " + o.toString() + " as " + arg.toString());
        if (o instanceof Manor && arg instanceof EnvReport){
            EnvReport rep = (EnvReport) arg;
            if (rep.diama != null)
                fg.addActor(rep.diama);
            if (rep.dirta != null)
                bg.addActor(rep.dirta);
        } else if (o instanceof Aspirobot){
            if(arg instanceof SelfActor){
                this.addActor((Actor) arg);
                ((Actor) arg).toFront();
            }
        }
    }
}
