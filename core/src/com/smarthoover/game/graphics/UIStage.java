package com.smarthoover.game.graphics;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.utils.viewport.Viewport;
import com.smarthoover.game.environment.Manor;
import com.smarthoover.game.utils.EnvReport;
import com.smarthoover.game.utils.TextureUtils;

import java.util.Observable;
import java.util.Observer;

import static com.smarthoover.game.utils.GameUtils.*;

public class UIStage extends Stage implements Observer {
    private final Group bg = new Group();
    private final Group fg = new Group();
    private final Label.LabelStyle ls = new Label.LabelStyle(LUCIDIA_SANS_TW, Color.GOLD);
    private final Label total = new Label(UI_LABEL_TOTAL + 0, ls);
    private final Label elec_spen = new Label(UI_LABEL_ELEC + 0, ls);
    private final Label dirt_dens = new Label(UI_LABEL_DDENS+ 0, ls);
    private final Label dirt_clea = new Label(UI_LABEL_DCLEA+ 0, ls);
    private final Label diam_lost = new Label(UI_LABEL_DLOST+ 0, ls);
    private final Label diam_save = new Label(UI_LABEL_DRET+ 0, ls);
    private final Image ui_bg = TextureUtils.texturedImg("ui_texture_LD.png");

    private void init(){
        this.addActor(bg);
        this.addActor(fg);
        this.getViewport().setScreenBounds(500,0,200,500);
        ui_bg.setPosition(500,0);
        total.setPosition(510, 450);
        elec_spen.setPosition(510, 430);
        dirt_dens.setPosition(510, 410);
        dirt_clea.setPosition(510, 390);
        diam_lost.setPosition(510, 370);
        diam_save.setPosition(510, 350);
        bg.addActor(ui_bg);
        fg.addActor(total);
        fg.addActor(elec_spen);
        fg.addActor(dirt_dens);
        fg.addActor(dirt_clea);
        fg.addActor(diam_lost);
        fg.addActor(diam_save);
    }

    public UIStage(){
        super();
        init();
    }


    public UIStage(Viewport vp){
        super(vp);
        init();
    }

    public UIStage(Viewport vp, Batch b){
        super(vp, b);
        init();
    }

    @Override
    public void update(Observable o, Object arg) {
        if (o instanceof Manor && arg instanceof EnvReport){
            EnvReport rep = (EnvReport) arg;
            this.total.setText(UI_LABEL_TOTAL + rep.score);
            this.elec_spen.setText(UI_LABEL_ELEC + rep.electricity_spent);
            this.dirt_dens.setText(UI_LABEL_DDENS + rep.dirt_density + '%');
            this.dirt_clea.setText(UI_LABEL_DCLEA + rep.dirt_cleaned);
            this.diam_lost.setText(UI_LABEL_DLOST + rep.diamonds_lost);
            this.diam_save.setText(UI_LABEL_DRET + rep.diamonds_saved);
        }
    }
}
