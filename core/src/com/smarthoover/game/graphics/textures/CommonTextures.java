package com.smarthoover.game.graphics.textures;

import com.badlogic.gdx.graphics.Texture;

public enum CommonTextures {
    INSTANCE;
    public final Texture DIRT_TEXTURE = new Texture("dirt_LD.png");
    public final Texture DIAMOND_TEXTURE = new Texture("diamond_LD.png");
    public final Texture ROBOT_TEXTURE = new Texture("robot_LD.png");
    public final Texture UI_BACKGROUND = new Texture("ui_texture_LD.png");

    public void dispose(){
        DIAMOND_TEXTURE.dispose();
        DIRT_TEXTURE.dispose();
    }
}
