package com.smarthoover.game.agent.objects;

import com.smarthoover.game.agent.Aspirobot;
import com.smarthoover.game.agent.State;
import com.smarthoover.game.environment.Manor;
import com.smarthoover.game.utils.Point;

public class Sensors {
    private final Manor env;
    private final Aspirobot owner;

    public Sensors(Manor env, Aspirobot owner){
        this.env = env;
        this.owner = owner;
    }

    public State senseForObjs(){
        State state = new State();
        Point tmp = null;
        for (int x = 0; x < 500; x += 50){
            for (int y = 0; y < 500; y += 50){
                tmp = new Point(x, y);
                if (env.GRID.has_diamond(x, y))
                    state.diamond.add(tmp);
                if(env.GRID.has_dirt(x, y))
                    state.dirt.add(tmp);
            }
        }
        state.aspirobot = this.getCurrPosition();
        return state;
    }
    public float sensePerfromance(){
        return env.currPerfomance();
    }

    public Point getCurrPosition(){
        return owner.getPosition();
    }
}
