package com.smarthoover.game.agent.objects;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.smarthoover.game.graphics.textures.CommonTextures;
import com.smarthoover.game.utils.Point;

public class SelfActor extends Actor{
    private Texture _tex;

    public SelfActor(){
        super();
        _tex = CommonTextures.INSTANCE.ROBOT_TEXTURE;
    }

    public SelfActor(int x, int y){
        super();
        _tex = CommonTextures.INSTANCE.ROBOT_TEXTURE;
        this.setX(x);
        this.setY(y);
    }

    public void setPosition(Point p){
        setPosition(p.x, p.y);
    }

    @Override
    public void draw(Batch batch, float alpha){
        batch.draw(_tex ,this.getX(), this.getY());
    }
}
