package com.smarthoover.game.agent.objects;

import com.smarthoover.game.agent.Aspirobot;
import com.smarthoover.game.environment.Manor;
import com.smarthoover.game.utils.ActionsEnum;
import com.smarthoover.game.utils.Point;


public class Effectors {
    private final Manor target;
    private final Aspirobot owner;

    public Effectors(Manor target, Aspirobot owner){
        this.target = target;
        this.owner = owner;
    }

    public void cleaner(){
        target.cleaned(owner.getPosition().x, owner.getPosition().y);
    }

    public void grabber(){
        target.grabbed(owner.getPosition().x, owner.getPosition().y);
    }

    // go left right up or down
    public void step(ActionsEnum action){
        Point p = owner.getPosition();
        switch (action){
            case RIGHT:
                p.x += 50;
                target.moved();
                owner.setPosition(p);
                break;
            case LEFT:
                p.x -= 50;
                target.moved();
                owner.setPosition(p);
                break;
            case UP:
                p.y += 50;
                target.moved();
                owner.setPosition(p);
                break;
            case DOWN:
                p.y -= 50;
                target.moved();
                owner.setPosition(p);
                break;
            default:
        }
    }
}
