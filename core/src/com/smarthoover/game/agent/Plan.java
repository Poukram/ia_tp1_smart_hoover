package com.smarthoover.game.agent;

import com.smarthoover.game.utils.ActionsEnum;
import com.smarthoover.game.utils.Point;

import java.util.ArrayList;
import java.util.Collections;

import static com.smarthoover.game.agent.GenericSearchTree.GenericSearchTreeNode;
import static com.smarthoover.game.utils.ActionsEnum.*;

public class Plan {
    ArrayList<ActionsEnum> orderedActions;

    Plan() {
        orderedActions = null;
    }

    private void buildPath(Point p1, Point p2){
        int tmp_x = p1.x;
        int tmp_y = p1.y;

        while (tmp_x != p2.x) {
            if (tmp_x > p2.x) {
                tmp_x -= 50;
                orderedActions.add(RIGHT);
            } else if (tmp_x < p2.x) {
                tmp_x += 50;
                orderedActions.add(LEFT);
            }
        }
        while (tmp_y != p2.y){
            if (tmp_y > p2.y){
                tmp_y -= 50;
                orderedActions.add(UP);
            } else if (tmp_y < p2.y){
                tmp_y += 50;
                orderedActions.add(DOWN);
            }
        }
    }
    public void buildPlan(GenericSearchTreeNode result){
        orderedActions = new ArrayList<ActionsEnum>();
        while(result.parent != null){
            if (result.action == CLEAN){
                orderedActions.add(SUCK);
                if (result.parent.state.hasDiamondAt(result.state.aspirobot) != null)
                    orderedActions.add(GRAB);
                buildPath(result.state.aspirobot, result.parent.state.aspirobot);
                result = result.parent;
            }
        }
        Collections.reverse(orderedActions);
    }
}
