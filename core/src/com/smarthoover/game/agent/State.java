package com.smarthoover.game.agent;

import com.smarthoover.game.utils.Point;

import java.util.ArrayList;
import java.util.List;

public class State {
    public List<Point> dirt;
    public List<Point> diamond;
    public Point aspirobot;

    public State(){
        diamond = new ArrayList<Point>();
        dirt = new ArrayList<Point>();
        aspirobot = null;
    }

    public State(State s){
        diamond =  new ArrayList<Point>();
        dirt = new ArrayList<Point>();
        for (Point p: s.diamond) {
            diamond.add(new Point(p));
        }
        for (Point p: s.dirt) {
            dirt.add(new Point(p));
        }
        aspirobot = new Point(s.aspirobot);
    }

    public Point hasDiamondAt(Point p){
        for(Point t : this.diamond){
            if (t.x == p.x && t.y == p.y)
                return t;
        }
        return null;
    }

    public Point hasDirtdAt(Point p){
        for(Point t : this.dirt){
            if (t.x == p.x && t.y == p.y)
                return t;
        }
        return null;
    }

    public void removeDiamondAt(Point p){
        Point pt = this.hasDiamondAt(p);
        if (pt != null)
            this.diamond.remove(hasDiamondAt(pt));
    }

    public void removeDirtAt(Point p){
        Point pt = this.hasDirtdAt(p);
        if (pt != null)
            this.dirt.remove(hasDirtdAt(pt));
    }

    public boolean equals(State s){
        for(Point p : this.dirt){
            if (!s.dirt.contains(p))
                return false;
        }
        for(Point p : this.diamond){
            if (!s.diamond.contains(p))
                return false;
        }
        if (!this.aspirobot.equals(s.aspirobot))
            return false;
        return true;
    }
}
