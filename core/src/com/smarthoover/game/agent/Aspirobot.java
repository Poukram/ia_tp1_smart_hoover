package com.smarthoover.game.agent;

import com.smarthoover.game.agent.objects.Effectors;
import com.smarthoover.game.agent.objects.SelfActor;
import com.smarthoover.game.agent.objects.Sensors;
import com.smarthoover.game.environment.Manor;
import com.smarthoover.game.utils.ActionsEnum;
import com.smarthoover.game.utils.Point;

import java.util.ArrayList;
import java.util.Observable;
import java.util.Random;

import static com.smarthoover.game.agent.GenericSearchTree.GenericSearchTreeNode;
import static com.smarthoover.game.utils.GameUtils.OBSERVATION_DELAY_STEPS;
import static com.smarthoover.game.utils.GameUtils.OBSERVATION_PERIOD_STEPS;
import static com.smarthoover.game.utils.StrategyEnum.BFS;
import static com.smarthoover.game.utils.StrategyEnum.GREEDY;
import static com.smarthoover.game.utils.StrategyEnum.UCS;

//A nice name for out agent
public class Aspirobot extends Observable implements Runnable {
    private final SelfActor _irobot;
    private final Belief belief;
    private final Plan desire;
    private final Manor env;
    private final Sensors sensors;
    private final Effectors effectors;
    private final Random rnd;
    private boolean alive;
    private boolean intention;
    private boolean has_reexplored;
    private Point position;
    private int obs_steps;
    private int steps_until_reexp;
    private int delay_steps;
    private float reexploration_freq;

    public Aspirobot(Manor env){
        this.env = env;
        obs_steps = 0;
        delay_steps = 0;
        reexploration_freq = 0;
        alive = false;
        belief = new Belief();
        desire = new Plan();
        intention = false;
        position = new Point(0, 0);
        _irobot = new SelfActor(position.x, position.y);
        rnd = new Random();
        this.sensors = new Sensors(env, this);
        this.effectors = new Effectors(env, this);
    }

    private void observeEnvironmentAndUpdateState(){
        belief.state = sensors.senseForObjs();
        belief.performance = sensors.sensePerfromance();
    }

    private void chooseAnAction(){
        // explore env with breadth first search or Greedy Search and choose path to all clean
        if (belief.state.dirt.size() > 0){
            GenericSearchTreeNode search_result = GenericSearchTree.TreeSearch(belief.state, UCS);
            if (search_result != null){
                intention = true;
                desire.buildPlan(search_result);
            }
        }
    }

    private void justDoIt(){
        if (!desire.orderedActions.isEmpty()){
            ActionsEnum todo = desire.orderedActions.get(0);
            desire.orderedActions.remove(0);
            switch (todo){
                case GRAB:
                    effectors.grabber();
                    break;
                case SUCK:
                    effectors.cleaner();
                    break;
                default:
                    effectors.step(todo);
            }
        } else
            intention = false;
    }

    public static boolean goalTest(GenericSearchTreeNode node) {
        return node.action != null && node.state.dirt.isEmpty();
    }

    public static ArrayList<State> successorFn(GenericSearchTreeNode node, ActionsEnum action){
        ArrayList<State> result_list = new ArrayList<State>();
        State curr_state = node.state;

        switch (action){
            case CLEAN:
                for (Point p : curr_state.dirt){
                    State tmp = new State(curr_state);
                    Point tmp_pt = tmp.hasDiamondAt(p);
                    if (tmp_pt != null)
                        tmp.diamond.remove(tmp_pt);
                    tmp.removeDirtAt(p);
                    tmp.aspirobot = new Point(p);
                    result_list.add(tmp);
                }
                break;
            default:
                break;
        }
        return result_list;
    }

    public static int stepCost(GenericSearchTreeNode parent, GenericSearchTreeNode successor, ActionsEnum action){
        int tmp = Point.manhattanDist(parent.state.aspirobot, successor.state.aspirobot);
        int nb_action = 1; // default case, only dirt on the grid
        if (parent.state.hasDiamondAt(successor.state.aspirobot) != null)
            nb_action++; // if grabbing the diamond is needed
        return (tmp/50) + nb_action;
    }

    public void setPosition(Point p){
        position.x = p.x;
        position.y = p.y;
        _irobot.setPosition(p);
    }

    public Point getPosition(){
        return position;
    }

    @Override
    public void run() {
        if (!alive){
            setChanged();
            notifyObservers(_irobot);
            alive = true;
        }
        learn_best_freq();
        if (!intention){
            observeEnvironmentAndUpdateState();
            chooseAnAction();
        } else
            justDoIt();
    }

    // FIXME actually learn a value instead of hardcoding one
    private void learn_best_freq() {
        if (delay_steps < OBSERVATION_DELAY_STEPS)
            delay_steps++;
        else{
            reexploration_freq = 105;
            if (intention){
                steps_until_reexp++;
                if (steps_until_reexp >= 15){
                    intention = false;
                    steps_until_reexp = 0;
                }
            } else if (steps_until_reexp > 0)
                steps_until_reexp = 0;
        }
    }
}
