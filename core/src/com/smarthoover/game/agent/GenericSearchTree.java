package com.smarthoover.game.agent;

import com.smarthoover.game.utils.ActionsEnum;
import com.smarthoover.game.utils.Point;
import com.smarthoover.game.utils.StrategyEnum;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

class GenericSearchTree {
    public static GenericSearchTreeNode TreeSearch(State problem, StrategyEnum strategy) {
        ArrayList<GenericSearchTreeNode> fringe = new ArrayList<GenericSearchTreeNode>();
        fringe.add(new GenericSearchTreeNode(problem));

        while (true) {
            if (fringe.isEmpty())
                return null;
            // Remove front here we use BFS so we expand the first element
            GenericSearchTreeNode to_expand = fringe.get(0);
            fringe.remove(0);
            if (Aspirobot.goalTest(to_expand))
                return to_expand;
            switch (strategy) {
                case BFS:
                    fringe.addAll(BFSExpand(to_expand));
                    break;
                case GREEDY:
                    fringe.add(GREEDYExpand(to_expand));
                    Collections.sort(fringe, new TMDComparator());
                    break;
                case UCS:
                    fringe.add(UCSExpand(to_expand));
                    Collections.sort(fringe, new UCSComparator());
                default:
            }
        }
    }

    private static GenericSearchTreeNode UCSExpand(GenericSearchTreeNode node){
        ArrayList<GenericSearchTreeNode> candidates = BFSExpand(node);
        UCSComparator ucs = new UCSComparator();
        Collections.sort(candidates, ucs);

        return candidates.get(0);
    }

    private static GenericSearchTreeNode GREEDYExpand(GenericSearchTreeNode node){
        ArrayList<GenericSearchTreeNode> candidates = BFSExpand(node);
        TMDComparator tmd = new TMDComparator();
        Collections.sort(candidates, tmd);

        return candidates.get(0);
    }

    private static class UCSComparator implements Comparator<GenericSearchTreeNode> {
        @Override
        public int compare(GenericSearchTreeNode o1, GenericSearchTreeNode o2) {
           return o1.cost - o2.cost;
        }
    }

    private static int TotalManhattanDistance(GenericSearchTreeNode node){
        int tmd = 0;
        for (Point p : node.state.dirt)
            tmd += Point.manhattanDist(p, node.state.aspirobot);
        return tmd;
    }

    private static class TMDComparator implements Comparator<GenericSearchTreeNode> {
        @Override
        public int compare(GenericSearchTreeNode o1, GenericSearchTreeNode o2) {
            int o1_dist = TotalManhattanDistance(o1);
            int o2_dist = TotalManhattanDistance(o2);
            return o1_dist - o2_dist;
        }
    }

    private static ArrayList<GenericSearchTreeNode> BFSExpand(GenericSearchTreeNode node){
        ArrayList<State> result;
        ArrayList<GenericSearchTreeNode> candidates = new ArrayList<GenericSearchTreeNode>();
        for (ActionsEnum action: ActionsEnum.values()) {
            result = Aspirobot.successorFn(node, action);
            for (State state : result){
                GenericSearchTreeNode s = new GenericSearchTreeNode(state);
                s.parent = node;
                s.action = action;
                s.cost = node.cost + Aspirobot.stepCost(node, s, action);
                s.depth = node.depth + 1;
                candidates.add(s);
            }
        }
        return candidates;
    }

    public static class GenericSearchTreeNode{
        public GenericSearchTreeNode parent;
        public ActionsEnum action;
        public State state;
        public int cost;
        public int depth;

        public boolean equals(GenericSearchTreeNode n){
            if(parent.equals(n.parent) && state.equals(n.state))
                return true;
            return false;
        }

        public GenericSearchTreeNode(State problem){
            parent = null;
            action = null;
            state = problem;
            cost = 0;
            depth = 0;
        }
    }
}
