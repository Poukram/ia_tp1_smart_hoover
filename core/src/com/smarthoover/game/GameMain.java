package com.smarthoover.game;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.assets.loaders.BitmapFontLoader;
import com.badlogic.gdx.assets.loaders.FileHandleResolver;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.utils.viewport.ScreenViewport;
import com.smarthoover.game.agent.Aspirobot;
import com.smarthoover.game.environment.Manor;
import com.smarthoover.game.graphics.ManorStage;
import com.smarthoover.game.graphics.UIStage;
import com.smarthoover.game.graphics.textures.CommonTextures;
import com.smarthoover.game.utils.CustomThreadPoolExecutor;
import com.smarthoover.game.utils.GameUtils;
import com.smarthoover.game.utils.TextureUtils;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import static com.smarthoover.game.utils.GameUtils.*;

public class GameMain extends ApplicationAdapter {
	private final ScheduledExecutorService se_service = new CustomThreadPoolExecutor(5);
	private ManorStage stage;
	private UIStage ui_stage;
	private Manor manor;
	private Aspirobot aspi;
	private Image floor;
	private Image grid;

	@Override
	public void create () {
	    System.out.println("Instantiating CommonTextures singleton " + CommonTextures.INSTANCE);
		stage = new ManorStage(new ScreenViewport());
		ui_stage = new UIStage(new ScreenViewport());
		manor = new Manor();
		aspi = new Aspirobot(manor);

		manor.addObserver(stage);
		aspi.addObserver(stage);
		manor.addObserver(ui_stage);

		floor = TextureUtils.tiledTexturedImg("wood_floor_seamless_LD.png",5);
		grid = TextureUtils.tiledTexturedImg("grid.png", 10);

        stage.addActor(floor);
        stage.addActor(grid);
        floor.setZIndex(0);
        grid.setZIndex(1);

		se_service.scheduleAtFixedRate(manor, 0,
				(long) (TIME_STEP * TIME_SEC_TO_NANO), TimeUnit.NANOSECONDS);
        se_service.scheduleAtFixedRate(aspi, 0,
                (long) (ROBOT_TIME_STEP * TIME_SEC_TO_NANO), TimeUnit.NANOSECONDS);
	}

	@Override
	public void render () {
		Gdx.gl.glClearColor(1, 0, 0, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		stage.act(Gdx.graphics.getDeltaTime());
		ui_stage.act(Gdx.graphics.getDeltaTime());
		stage.draw();
		ui_stage.draw();
	}
	
	@Override
	public void dispose () {
	    stage.dispose();
		CommonTextures.INSTANCE.dispose();
        GameUtils.dispose();
	}
}
