package com.smarthoover.game.environment;

import com.badlogic.gdx.utils.TimeUtils;
import com.smarthoover.game.environment.objects.DiamondActor;
import com.smarthoover.game.environment.objects.DirtActor;
import com.smarthoover.game.utils.EnvReport;

import java.security.SecureRandom;
import java.util.Observable;
import java.util.Random;

import static com.smarthoover.game.utils.GameUtils.*;

// A nice name for our environment
public class Manor extends Observable implements Runnable{

    public final ManorGrid GRID = new ManorGrid();

    private float _manor_dirt_density;
    private long _diamonds_lost;
    private long _diamonds_retrieved;
    private long  _electricity_spent;
    private long _dirt_cleaned;
    private long _former_time = TimeUtils.millis();
    private DiamondActor new_diam;
    private DirtActor new_dirt;

    private final Random sec_rnd = new SecureRandom();

    public Manor(){
        _manor_dirt_density = 0.0f;
        _diamonds_lost = 0;
        _diamonds_retrieved = 0;
        _electricity_spent = 0;
        _dirt_cleaned = 0;
    }

    private void density_update(){
        _manor_dirt_density = GRID.getDirt_qty();
    }

    private boolean shouldGenNewDirt(){
        return sec_rnd.nextFloat() >= 1.0f - (OBJ_SPAWN_PER_SEC * TIME_STEP);
    }

    private boolean shouldGenNewDiamond(){
        return sec_rnd.nextFloat() >= 1.0f - ((OBJ_SPAWN_PER_SEC * TIME_STEP)
                / DIAMOND_PROB_DIVIDER);
    }

    private synchronized void generateDirt(){
        int x = sec_rnd.nextInt(10) * 50;
        int y = sec_rnd.nextInt(10) * 50;
        for (DirtActor da : GRID.dirt_actors){
            if (da != null && (int) da.getX() == x && (int) da.getY() == y && GRID.getDirt_qty() < 100){
                generateDirt();
                return;
            }
        }
        DirtActor tmp = new DirtActor(x, y);
        GRID.add(tmp);
        new_dirt = tmp;
        this.setChanged();
    }

    private synchronized void generateDiamond(){
        int x = sec_rnd.nextInt(10) * 50;
        int y = sec_rnd.nextInt(10) * 50;
        for (DiamondActor da : GRID.diamond_actors){
            if (da != null && (int) da.getX() == x && (int) da.getY() == y && GRID.getDiam_qty() < 100){
                generateDiamond();
                return;
            }
        }
        DiamondActor tmp = new DiamondActor(x, y);
        GRID.add(tmp);
        new_diam = tmp;
        this.setChanged();
    }

    public synchronized void moved(){
        _electricity_spent++;
        setChanged();
    }

    public synchronized void grabbed(float x, float y){
        _electricity_spent++;
        if(GRID.has_diamond(x, y))
            _diamonds_retrieved++;
        GRID.remove_diamond(x, y);
        setChanged();
    }

    public synchronized void cleaned(float x, float y){
        _electricity_spent++;
        if (GRID.has_dirt(x, y))
            _dirt_cleaned++;
        if (GRID.has_diamond(x, y))
            _diamonds_lost++;
        GRID.remove(x, y);
        setChanged();
    }

    public synchronized float currPerfomance(){
        return (_manor_dirt_density / 100 * CLEANLINESS) + (_diamonds_lost * DIAMOND_LOSS)
                + (_diamonds_retrieved * DIAMOND_RESCUE) + (_dirt_cleaned * DIRT_CLEAN)
                - _electricity_spent;
    }

    @Override
    public void run() {
        new_diam = null;
        new_dirt = null;
        if(shouldGenNewDiamond() && GRID.getDiam_qty() < 100){
            generateDiamond();
        }
        if(shouldGenNewDirt() && GRID.getDirt_qty() < 100){
            generateDirt();
        }
        if(this.hasChanged()){
            density_update();
            this.notifyObservers(new EnvReport(currPerfomance(),_manor_dirt_density, _diamonds_lost,
                    _diamonds_retrieved, _dirt_cleaned, _electricity_spent, new_dirt, new_diam));
        }
        _former_time = TimeUtils.millis();
    }
}
