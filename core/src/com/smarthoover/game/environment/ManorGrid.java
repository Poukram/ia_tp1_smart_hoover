package com.smarthoover.game.environment;

import com.badlogic.gdx.scenes.scene2d.Actor;
import com.smarthoover.game.environment.objects.DiamondActor;
import com.smarthoover.game.environment.objects.DirtActor;

public class ManorGrid {
    public final DiamondActor[] diamond_actors = new DiamondActor[100];
    public final DirtActor[] dirt_actors = new DirtActor[100];
    private int dirt_qty;
    private int diam_qty;

    public ManorGrid(){
        dirt_qty = 0;
        diam_qty = 0;
    }

    private int index(float x, float y){
        return (int) (x / 50.0f + (y / 5.0f));
    }

    public int getDiam_qty(){
        return diam_qty;
    }

    public int getDirt_qty(){
        return dirt_qty;
    }

    public boolean has_diamond(float x, float y){
        return diamond_actors[index(x, y)] != null;
    }

    public boolean has_dirt(float x, float y){
        return dirt_actors[index(x, y)] != null;
    }

    public void add(DiamondActor a){
        diamond_actors[index(a.getX(), a.getY())] = a;
        diam_qty++;
    }

    public void add(DirtActor a){
        dirt_actors[index(a.getX(), a.getY())] = a;
        dirt_qty++;
    }

    public void remove(float x, float y){
        remove((int)x, (int)y);
    }

    public void remove(int x, int y){
        remove_diamond(x, y);
        remove_dirt(x, y);
    }

    public void remove_diamond(float x, float y){
        remove_diamond((int)x, (int)y);
    }

    public void remove_diamond(int x, int y){
        Actor a = diamond_actors[index(x, y)];
        if (a != null){
            a.remove();
            diamond_actors[index(x, y)] = null;
            diam_qty--;
        }
    }

    public void remove_dirt(float x, float y){
        remove_dirt((int)x, (int)y);
    }

    public void remove_dirt(int x, int y){
        Actor a = dirt_actors[index(x, y)];
        if (a != null){
            a.remove();
            dirt_actors[index(x, y)] = null;
            dirt_qty--;
        }
    }
}
