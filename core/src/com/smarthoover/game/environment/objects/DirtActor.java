package com.smarthoover.game.environment.objects;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.smarthoover.game.graphics.textures.CommonTextures;

public class DirtActor extends Actor{
    private Texture _tex;

    public DirtActor(){
        super();
        _tex = CommonTextures.INSTANCE.DIRT_TEXTURE;
    }

    public DirtActor(float x, float y){
        super();
        _tex = CommonTextures.INSTANCE.DIRT_TEXTURE;
        this.setX(x);
        this.setY(y);
    }

    @Override
    public void draw(Batch batch, float alpha){
        batch.draw(_tex ,this.getX(), this.getY());
    }
}
