package com.smarthoover.game.utils;

import com.smarthoover.game.environment.objects.DiamondActor;
import com.smarthoover.game.environment.objects.DirtActor;

public class EnvReport {
    public final float score;
    public final float dirt_density;
    public final long diamonds_lost;
    public final long diamonds_saved;
    public final long electricity_spent;
    public final long dirt_cleaned;
    public final DirtActor dirta;
    public final DiamondActor diama;

    public EnvReport(float s, float dd, long dl, long dr, long dc, long es, DirtActor dirta, DiamondActor diama){
        score = s;
        dirt_density = dd;
        diamonds_lost = dl;
        diamonds_saved = dr;
        dirt_cleaned = dc;
        electricity_spent = es;
        this.dirta = dirta;
        this.diama = diama;
    }
}
