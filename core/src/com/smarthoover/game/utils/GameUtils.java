package com.smarthoover.game.utils;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.BitmapFont;

import static java.lang.Math.pow;

public class GameUtils {
    public static final float OBSERVATION_PERIOD = 20.0f; //seconds
    public static final float OBSERVATION_DELAY = 3.0f; //seconds
    public static final int CLEANLINESS = -256;
    public static final byte DIAMOND_LOSS = -8;
    public static final char DIAMOND_RESCUE = 4;
    public static final char DIRT_CLEAN = 6;
    public static final float TIME_STEP = 1.0f / 60.0f;
    public static final float ROBOT_TIME_STEP = 1.0f/ 20.0f;
    public static final int OBSERVATION_PERIOD_STEPS = (int) ( OBSERVATION_PERIOD / ROBOT_TIME_STEP);
    public static final int OBSERVATION_DELAY_STEPS = (int) ( OBSERVATION_DELAY / ROBOT_TIME_STEP);
    public static final double TIME_SEC_TO_NANO = pow(10, 9);
    public static final float OBJ_SPAWN_PER_SEC = 2.5f;
    public static final char DIAMOND_PROB_DIVIDER = 6;
    public static final String UI_LABEL_TOTAL = "Total score   ";
    public static final String UI_LABEL_ELEC =  "Electricity   ";
    public static final String UI_LABEL_DLOST = "Diamond lost  ";
    public static final String UI_LABEL_DRET =  "Diamond saved ";
    public static final String UI_LABEL_DDENS = "Dirt density  ";
    public static final String UI_LABEL_DCLEA = "Dirt cleaned  ";
    public static final BitmapFont LUCIDIA_SANS_TW =
            new BitmapFont(Gdx.files.internal("lucidia_sans_tw_16_bold.fnt"));

    private GameUtils(){
    }

    public static void dispose(){
        LUCIDIA_SANS_TW.dispose();
    }
}
