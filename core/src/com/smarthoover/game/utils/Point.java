package com.smarthoover.game.utils;

import static java.lang.Math.abs;

public class Point {
    public int x;
    public int y;

    public static int manhattanDist(Point p1, Point p2){
        return abs(p2.x - p1.x) + abs(p2.y - p1.y);
    }

    public Point(int x, int y){
        this.x = x;
        this.y = y;
    }

    public Point(Point p) {
        this.x = p.x;
        this.y = p.y;
    }

    public boolean equals(Point p){
        if (this.x == p.x && this.y == p.y)
            return true;
        return false;
    }
}
