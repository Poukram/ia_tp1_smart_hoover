package com.smarthoover.game.utils;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;

import static com.badlogic.gdx.graphics.Texture.TextureWrap.Repeat;

public enum TextureUtils {
    INSTANCE;

    public static Image texturedImg(String path){
        return tiledTexturedImg(path, 1);
    }
    public static Image tiledTexturedImg(String path, int repeatFactor){

        Image tmp_img = new Image();
        Texture tmp_tex = new Texture(path);
        tmp_tex.setWrap(Repeat, Repeat);
        TextureRegion floor_tex_reg = new TextureRegion(tmp_tex);
        floor_tex_reg.setRegion(0,0, tmp_tex.getWidth() * repeatFactor,
                tmp_tex.getHeight() * repeatFactor);
        TextureRegionDrawable floor_tex_reg_D = new TextureRegionDrawable(floor_tex_reg);
        tmp_img.setDrawable(floor_tex_reg_D);
        tmp_img.setSize(floor_tex_reg.getRegionWidth(),floor_tex_reg.getRegionHeight());
        return tmp_img;
    }
}
