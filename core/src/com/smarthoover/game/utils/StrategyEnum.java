package com.smarthoover.game.utils;

public enum StrategyEnum {
    BFS, GREEDY, UCS
}
