package com.smarthoover.game.utils;

public enum ActionsEnum {
    CLEAN, SUCK, GRAB, STANDBY, RIGHT, LEFT, UP, DOWN
}
